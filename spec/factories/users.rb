# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    name { FFaker::Name.name }
    email { FFaker::Email.email }
    password { FFaker::Password.password }
  end
end
